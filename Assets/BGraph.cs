﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGraph : MonoBehaviour {
    [SerializeField] Material m;
	public IEnumerator Begin (Graph graph, AStar s, Node start, Node end, int heuristicWeight_, int MAX_X, int MAX_Z) {
        mx = MAX_X;
        mz = MAX_Z;
        heuristicWeight = heuristicWeight_;

        Dictionary<Node, GameObject> map = new Dictionary<Node, GameObject>();
        ///CREATE PHYSICAL GRID
        int length = graph.map.Count;
        int j = 0;
        foreach(Node n in graph.map.Values)
        {
            StartCoroutine(GenerateGameObject(n, map, graph));
            j++;
            if(j % (length/100) == 0)
                yield return null;
        }

        ///SHOW START/END
        yield return new WaitForSeconds(1);
        float SHOW_START_END_TIME = 30;
        GameObject startGO, endGO;
        startGO = map[start];
        endGO = map[end];
        for(int i = 0; i <= SHOW_START_END_TIME; i++)
        {
            startGO.GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.green, i / SHOW_START_END_TIME);
            endGO  .GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.red  , i / SHOW_START_END_TIME);
            yield return null;
        }
        yield return new WaitForSeconds(1);

        //Allow redistribution of grid
        while(!Input.GetKeyDown(KeyCode.Space))
        {
            RaycastHit rh;
            Camera c = Camera.main;
            if (Physics.Raycast(c.ScreenPointToRay(Input.mousePosition), out rh, 1000, 1 << 8))
            {
                if(Input.GetMouseButton(0))
                {
                    string name = rh.transform.gameObject.name;
                    graph.map[name].w = 5;


                    if (coroutines.ContainsKey(name))
                        StopCoroutine(coroutines[name]);
                    else
                        coroutines.Add(name, null);
                    Coroutine co = StartCoroutine(GenerateGameObject(graph.map[name], map, graph));
                    coroutines[name] = co;
                }
                if (Input.GetMouseButton(1))
                {
                    string name = rh.transform.gameObject.name;
                    graph.map[name].w = int.MaxValue;


                    Coroutine co = StartCoroutine(GenerateGameObject(graph.map[name], map, graph));
                    if (coroutines.ContainsKey(name))
                        if (coroutines[name] != null)
                            StopCoroutine(coroutines[name]);
                        else { }
                    else
                        coroutines.Add(name, null);
                    coroutines[name] = co;
                }
                Debug.Log(rh.transform.gameObject.name);
            }
            yield return null;
        }

        s.SearchFor(start, end);

        ///DISPLAY HISTORY
        Queue<AStar.NodeHistory> history = s.getHistory();
        int guessCost = s.heuristic(start, end);
        yield return null;
        while(history.Count != 0)
        {
            yield return null;
            while (!Input.GetKey(KeyCode.Space))
                yield return null;
            StartCoroutine(showNodeChange(history, map, graph, guessCost, end));
            iteration++;
        }
    }
    IEnumerator showNodeChange(Queue<AStar.NodeHistory> history, Dictionary<Node, GameObject> map, Graph graph, float guessCost, Node end)
    {
        int iteration_ = iteration;
        AStar.NodeHistory nh = history.Dequeue();
        GameObject nGO = map[nh.n];
        nGO.GetComponent<Renderer>().material.color = Color.white;
        Color endColor = Color.Lerp(Color.green, Color.red, (nh.costToGetTo / guessCost / (graph.MAX_WEIGHT)));
        Renderer renderer = nGO.GetComponent<Renderer>();
        for (float t = 0; t <= 1; t += 0.05f)
        {
            float t2 = Mathf.Pow(t,2);
            renderer.material.color = Color.Lerp(Color.white, endColor, t2);
            yield return null;
        }
        yield return null;
        if (nh.n == end)
            console += "iteration: " + iteration_ + "\tcost: " + nh.costToGetTo + "\n";
    }
    Dictionary<string, Coroutine> coroutines = new Dictionary<string, Coroutine>();
    IEnumerator GenerateGameObject(Node n, Dictionary<Node, GameObject> map, Graph graph)
    {
        GameObject go;
        if (map.ContainsKey(n))
        {
            go = map[n];
        }
        else
        {
            go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            map.Add(n, go);
        }
        Vector3 endScale = new Vector3(1, 1.1f, 1);
        go.transform.position = new Vector3(n.x, 0, n.z);
        go.GetComponent<Renderer>().material = m;
        go.GetComponent<Renderer>().material.color = Color.gray;
        go.transform.parent = transform;
        go.layer = 8;
        go.name = n.id;

        /*
        GameObject child = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        child.transform.parent = go.transform;
        //child.transform.localPosition = Vector3.zero + Vector3.up*0.1f;
        child.transform.localScale = (Vector3.left+Vector3.forward) * 0.75f + Vector3.up*1.1f;
        child.GetComponent<Renderer>().material = m;
        child.GetComponent<Renderer>().material.color = Color.Lerp(Color.white,Color.black,n.w/(float)graph.MAX_WEIGHT);
        */

        int SCALE_TIME = 5;
        if(n.w != int.MaxValue)
        {
            for (int i = 0; i <= SCALE_TIME; i++)
            {
                go.transform.localScale = Vector3.Lerp(Vector3.zero, endScale, i / (float)SCALE_TIME);
                yield return null;
            }
            for (int i = 0; i < n.w; i++)
            {
                go.transform.Translate(Vector3.up / 10f);
                go.transform.localScale += Vector3.up / 10f;
                yield return null;
            }
        }
        else
        {
            go.transform.localScale = Vector3.one * 0.5f;
            go.GetComponent<Renderer>().material.color = Color.black;
        }
    }
    string console = "\t\t\n";
    int iteration = 0;
    float heuristicWeight;
    int mx, mz;
    void OnGUI()
    {
        GUILayout.Box("iteration:\t"+iteration+"\nX:\t"+mx+"\nZ:\t"+mz);
        GUILayout.Box("heuristic weight:\t" + heuristicWeight);
        GUILayout.FlexibleSpace();
        GUILayout.Box(console);
    }
}
