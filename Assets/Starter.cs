﻿using UnityEngine;
using System.Collections;

public class Starter : MonoBehaviour {
    [SerializeField] BGraph bg;
    public int MAX_X, MAX_Z;
    public int HEURISTIC_WEIGHT;
	void Start () {
        Graph g = new Graph(MAX_X, MAX_Z);
        Node start, end;
        do
        {
            start = g.getRandom();
            end = g.getRandom();
        }
        while (Mathf.Abs(start.x - end.x) < MAX_X * .75f || Mathf.Abs(start.z - end.z) < MAX_Z * .75f);

        AStar s = new AStar(g, HEURISTIC_WEIGHT);
        s.SearchFor(start, end);

        Debug.Log(g);
        GameObject.Find("Pivot").transform.position = Vector3.right * MAX_X / 2 + Vector3.forward * MAX_Z / 2;
        GameObject.Find("Camera").transform.localPosition = Vector3.up * MAX_X * 3f / 4f - Vector3.forward * MAX_X / 6f * 5f;
        StartCoroutine(bg.Begin(g, s, start, end, HEURISTIC_WEIGHT, MAX_X, MAX_Z));
	}
}
