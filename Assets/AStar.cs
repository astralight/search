﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
public class AStar
{
    Graph graph;
    public AStar(Graph graph_, float heuristicWeight_)
    {
        graph = graph_;
        heuristicWeight = heuristicWeight_;
    }

    Dictionary<Node, float> map = new Dictionary<Node, float>();
    Dictionary<Node, Node> prevs = new Dictionary<Node, Node>();
    Queue<NodeHistory> history = new Queue<NodeHistory>();
    public Queue<NodeHistory> getHistory() { return new Queue<NodeHistory>(history.ToList()); }
    public bool SearchFor(Node start, Node end)
    {
        map.Clear();
        prevs.Clear();
        history.Clear();


        List<Node> queue = new List<Node>();
        map.Add(start, 0);
        queue.Add(start);
        prevs.Add(start, null);

        float bestValueFoundSoFar = float.MaxValue;

        while(queue.Count != 0)
        {
            Node next;
            next = queue[0];
            queue.RemoveAt(0);
            float dist = 0;
            dist = map[next];
            if (dist == int.MaxValue) continue;
            history.Enqueue(new NodeHistory { n = next, costToGetTo = dist});
            foreach (Node adj in graph.getAdjacents(next))
            {
                if (adj.w == int.MaxValue) continue;
                if(!map.ContainsKey(adj))
                    map.Add(adj, float.MaxValue);
                if(dist + adj.w < map[adj])
                {
                    if (!map.ContainsKey(adj))
                        map.Add(adj, 0);
                    map[adj] = dist + adj.w;
                    if (!queue.Contains(adj))
                    {
                        bool found = false;
                        for (int i = 0; i < queue.Count; i++)
                        {
                            float currValue = queue[i].w + heuristic(queue[i], end) * heuristicWeight;
                            float prevValue = map[adj] + heuristic(adj, end) * heuristicWeight;
                            if (currValue > prevValue)
                            {
                                found = true;
                                queue.Insert(i, adj);
                                break;
                            }
                        }
                        if (!found)
                        {
                            queue.Add(adj);
                        }
                    }
                    if (!prevs.ContainsKey(adj))
                        prevs.Add(adj, null);
                    prevs[adj] = next;
                }
            }
        }
        return map.ContainsKey(end);
    }
    float heuristicWeight;
    public int heuristic(Node n, Node end)
    {
        return (int)Math.Sqrt(Math.Pow(n.x-end.x,2)+Math.Pow(n.z-end.z,2));
    }

    public class NodeHistory
    {
        public Node n;
        public float costToGetTo;
    }
}
