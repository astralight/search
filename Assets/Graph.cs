﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UER = UnityEngine.Random;

public class Graph
{
    public Dictionary<String, Node> map = new Dictionary<string, Node>();
    float missRate = 0.2f;
    public int MAX_WEIGHT {get; private set;}
    public Graph(int MAX_X, int MAX_Z)
    {
        MAX_WEIGHT = 5;
        {
            int i = 0;
            dirs[i++] = new int[] { -1,  0 };
            dirs[i++] = new int[] {  1,  0 };
            dirs[i++] = new int[] {  0, -1 };
            dirs[i++] = new int[] {  0,  1 };
        }

        for(int i = 0; i < MAX_X*MAX_Z; i++)
        {
            int ix, iz;
            ix = i / MAX_Z;
            iz = i % MAX_Z;
            Node n = new Node(ix, iz, UER.value >= missRate ? UER.Range(1,MAX_WEIGHT+1)*2 : int.MaxValue);
            map.Add(n.id, n);
        }
    }
    static int[][] dirs = new int[4][];
    public List<Node> getAdjacents(Node n)
    {
        List<Node> output = new List<Node>();
        if(map.ContainsKey(n.id))
        {
            int ix, iz;
            int r = UER.Range(0, 4);
            for(int i = 0; i < dirs.Length; i++)
            {
                int[] dir = dirs[(i + r) % dirs.Length];
                ix = n.x + dir[0];
                iz = n.z + dir[1];
                String id = Node.GetID(ix, iz);
                if (map.ContainsKey(id))
                    output.Add(map[id]);
            }
        }
        return output;
    }
    public Node getRandom()
    {
        return map.ElementAt(UER.Range(0, map.Count)).Value;
    }
}
