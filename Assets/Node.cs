﻿using UnityEngine;
using System.Collections;

public class Node {
    public int x, z, w;
    public Node(int x_, int z_) : this(x_, z_, 0) { }
    public Node(int x_, int z_, int w_)
    {
        x = x_;
        z = z_;
        w = w_;
    }
    private string _id;
    public string id { get { return _id == null ? GetID(x, z) : _id; } }
    public static string GetID(int x, int z)
    {
        return x + " " + z;
    }
}
